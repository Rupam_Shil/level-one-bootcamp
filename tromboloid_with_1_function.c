//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>
void main() {
printf("Program to find the volume of Tromboloid\n");
float h, b, d, vol;
printf("Please enter the H, D, B of the tromboloid");
scanf("%f%f%f", &h, &d, &b) ;
vol = ((h*b*d) + (d/b)) / 3;
printf("The volume of the tromboloid is %.2f", vol);
} 
