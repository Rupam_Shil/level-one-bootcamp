#include <stdio.h>
#include<math.h>
float show_input(){
float temp;
scanf("%f",&temp);
return temp;};

float calc_dist(float a, float b, float c, float d){
return (sqrt((pow((b-a),2)+(pow((d-c),2)))));
};

void show_output(float x){
printf("The distance between the points is %.2f",x);};

int main(){
float x1, x2, y1, y2,dist;
printf("For first point:\n");
printf("Enter the X:\n");
x1 = show_input();
printf("Enter the Y:\n");
y1 = show_input();
printf("For second point:\n");
printf("Enter the X:\n");
x2 =show_input();
printf("Enter the Y:\n");
y2 =show_input();
dist = calc_dist(x1, x2, y1, y2);
show_output(dist);

return 0;} 
