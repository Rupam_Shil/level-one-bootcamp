//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int take_input();
int calc_sum(int a, int b);
void show_output(int a, int b, int c);

void main(){
int no_1,no_2,sum;
no_1=take_input();
no_2=take_input();
sum=calc_sum(no_1,no_2);
show_output(no_1,no_2,sum);
}

int take_input(){
int no;
printf("Please enter the no:\n");
scanf("%d",&no);
return no;
}
int calc_sum(int a, int b){
return a +b;
}
void show_output(int a, int b, int c){
printf("The sum of %d and %d is: %d\n",a, b, c);
}