//Write a program to add two user input numbers using one function.
#include <stdio.h>

int main() {
int num_1, num_2,sum;
printf("Enter the two numbers: \n" ) ;
scanf("%d%d" , &num_1,&num_2);
sum=num_1+num_2;
printf("The sum of two number %d and %d is %d" , num_1,num_2,sum);
return 0;
} 
